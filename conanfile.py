#  vim: set expandtab fdm=marker ts=2 sw=2 tw=100 et :
from conans import ConanFile, CMake, tools

class AliceConan(ConanFile):
  name = "alice"
  version = "0.3"
  license = "MIT License"
  author = "Mathias Soeken"
  url = "https://github.com/ruanformigoni/alice"
  description = "Alice is a C++-14 command shell library"
  topics = ("Command-line", "Tool", "C++14", "C++17", "Python")
  exports = "*"
  generators = "cmake_find_package"

  def requirements(self):
    self.requires("cli11/1.6.1@bincrafters/stable")
    self.requires("nlohmann_json/3.8.0")
    self.requires("fmt/6.0.0@bincrafters/stable")
    self.requires("pybind11/2.3.0@conan/stable")

  def source(self):
    # self.run("git clone git@gitlab.com:formigoni/alice.git .")
    tools.replace_in_file(
      "include/alice/cli.hpp",
      "CLI11.hpp",
      '''alice/CLI11.hpp'''
    )
    tools.replace_in_file(
      "include/alice/command.hpp",
      "CLI11.hpp",
      '''alice/CLI11.hpp'''
    )
    tools.replace_in_file(
      "include/alice/validators.hpp",
      "CLI11.hpp",
      '''alice/CLI11.hpp'''
    )

  def build(self):
    cmake = CMake(self)
    cmake.configure()
    cmake.install()

  def package(self):
    self.copy("*.hpp", dst="include", src="include")
    self.copy("*.cmake", dst="lib", src="lib")
    self.copy("*.cmake", dst="tools", src="tools")

  def package_info(self):
    self.info.name = "alice"

    self.cpp_info.components["AlicePython"].names["cmake_find_package"] = "alice_python"
    self.cpp_info.components["AlicePython"].requires = ["fmt::fmt"]
    self.cpp_info.components["AlicePython"].requires = ["CLI11::CLI11"]
    self.cpp_info.components["AlicePython"].requires = ["pybind11::pybind11"]
    self.cpp_info.components["AlicePython"].requires = ["nlohmann_json::nlohmann_json"]
